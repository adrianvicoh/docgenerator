
# 1. Generador de documentos

Diseño de la base de datos de DocGen. Un generador de documentos.


# 2. Model conceptual
## 2.1. Enllaç públic a l'esquema
[Ver esquema](https://drive.google.com/file/d/1AW38a5pmmGrKcnVks5iyR_YrTLZUL3-o/view?usp=sharing)
## 2.2. Esquema conceptual (EC ó ER)
  ![7_cinefil](base_datos.png)
# 3. Model lògic relacional
## 3.1. Esquema lògic
  - usuario(**idUsuario**, nickname, nombre, apellido, avatar, password, email, <ins>idDoc, idRol</ins>)  
  - documento(**idDoc**, nombre, ruta, timestamp, <ins>idPlantilla</ins>)  
  - plantilla(**idPlantilla**, ruta, timestamp)   
  - formulario(**idPlantilla**, nombre, ruta, timestamp, <ins>idDocumento</ins>)  

## 3.2. Diagrama referencial

Relació referencial|Clau aliena|Relació referida
-|:-:|-
usuario|idDoc|documento 
usuario|idRol|rol 
documento|idPlantilla|plantilla    
formulario|idDocumento|documento      

# 4. Model físic
## 4.1 SQL

  
```sql

-- Crear la tabla de Rol
CREATE TABLE Rol (
id INT PRIMARY KEY,
rol VARCHAR(255)
);

-- Crear la tabla de Documento
CREATE TABLE Documento (
id INT PRIMARY KEY,
nombre VARCHAR(255),
ruta VARCHAR(255),
timestmp VARCHAR(255),
idPlantilla INT,
FOREIGN KEY (idPlantilla) REFERENCES Plantilla(id)
);

-- Crear la tabla de Usuario
CREATE TABLE Usuario (
id INT PRIMARY KEY,
nickname VARCHAR(255),
nombre VARCHAR(255),
apellido VARCHAR(255),
passwd VARCHAR(255),
avatar VARCHAR(255),
email VARCHAR(255),
idDoc INT,
idRol INT,
FOREIGN KEY (idDoc) REFERENCES Documento(id),
FOREIGN KEY (idRol) REFERENCES Rol(id)
);

-- Crear la tabla de Formulario
CREATE TABLE Formulario (
id INT PRIMARY KEY,
nombre VARCHAR(255),
ruta VARCHAR(255),
idDocumento INT,
timestmp VARCHAR(255),
FOREIGN KEY (idDocumento) REFERENCES Documento(id)
);

-- Crear la tabla de Plantilla
CREATE TABLE Plantilla (
id INT PRIMARY KEY,
ruta VARCHAR(255),
timestmp VARCHAR(255)
);

```