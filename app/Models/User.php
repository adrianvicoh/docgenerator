<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'rol_id',
        'email',
        'avatar',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // Relación muchos a uno con la tabla roles
    public function rol() {
        return $this->belongsTo(Rol::class);
    }

     // Relación uno a muchos con la tabla documents
     public function documents() {
        return $this->hasMany(Document::class, 'user_id');
    }

    // Método para obtener el identificador del usuario
    public function getAuthIdentifierName() {
        return 'id';
    }

    // Método para obtener el identificador del usuario
    public function getAuthIdentifier() {
        return $this->getKey();
    }

    // Método para obtener la contraseña del usuario
    public function getAuthPassword() {
        return $this->password;
    }

    // Método para obtener el token de autenticación de recuerdame
    public function getRememberToken() {
        return $this->remember_token;
    }

    // Método para establecer el token de autenticación de recuerdame
    public function setRememberToken($value) {
        $this->remember_token = $value;
    }

    // Método para obtener el nombre del campo de autenticación de recuerdame
    public function getRememberTokenName() {
        return 'remember_token';
    }
}
