<?php

namespace App\Models;

use App\Http\Requests\ProfileUpdateRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use Illuminate\Support\Facades\Http;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    protected $table = "forms";
    protected $fillable = [
        'name',
        'xml_path',
        'html_path',
        'image'
    ];

    // Relación uno a muchos con la tabla documentos
    public function documentos() {
        return $this->hasMany(Documento::class, 'form_id');
    }

    // Relación uno a muchos con la tabla templates
    public function templates() {
        return $this->hasMany(Template::class, 'form_id');
    }
}