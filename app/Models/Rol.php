<?php

namespace App\Models;

use App\Http\Requests\ProfileUpdateRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use Illuminate\Support\Facades\Http;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Rol extends Model
{
    use HasFactory;

    protected $table = "roles";

    protected $fillable = [
        'rol',
    ];

    // Relación uno a muchos con la tabla users
    public function users() {
        return $this->hasMany(User::class, 'rol_id');
    }

}