<?php

namespace App\Models;

use App\Http\Requests\ProfileUpdateRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use Illuminate\Support\Facades\Http;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Form;


class Template extends Model 
{
    protected $table = "templates";

    protected $fillable = [
        'name',
        'image',
        'route',
        'form_id',
    ];

    // Relación uno a muchos con la tabla documentos
    public function documentos() {
        return $this->hasMany(Documento::class, 'template_id');
    }

    // Relación de muchos a uno con la tabla Form
    public function form() {
        return $this->belongsTo(Form::class, 'form_id');
    }
}