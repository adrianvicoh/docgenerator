<?php

namespace App\Models;

use App\Http\Requests\ProfileUpdateRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use Illuminate\Support\Facades\Http;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Document extends Model 
{
    protected $table = "documents";
    protected $fillable = [
        'user_id',
        'template_id',
        'form_id',
        'name',
        'path_html',
        'path_xml'
    ];

    // Relación muchos a uno con la tabla usuers
    public function user() {
        return $this->belongsTo(User::class);
    }

}