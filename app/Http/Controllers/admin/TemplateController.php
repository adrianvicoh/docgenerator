<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use App\Models\Template;
use App\Models\Form;
use App\Http\Requests\TemplateRequest;

class TemplateController extends Controller
{
    public function index() {
        $templates = Template::all();
        return view('admin.templates.template-list', compact('templates'));
    }
    public function add(TemplateRequest $request) {
        // Almacenar el archivo
        $tipo = $request->input('tipoArchivo');
        $file = $request->file('file');
        $image = $request->file('image');
        $imgExtension = $image->getClientOriginalExtension();
        $fileExtension = $file->getClientOriginalExtension();
        $fName = $request->input('fileName');
        $fNameExt = $request->input('fileName').'.'.$fileExtension;
        $route = 'storage/docs/'.$tipo.'/templates'.'/'.$fName;
        $uploadRoute = 'public/docs/'.$tipo.'/templates'.'/'.$fName;
        
        //Guardamos los archivos
        $file->storeAs($uploadRoute, $fNameExt);
        $image->storeAs($uploadRoute, $fName.'.'.$imgExtension);
        
        //Creamos el registro en la base de datos
        $form = Form::where('name', $tipo)->first();
        Template::create([
            'name' => $fName,
            'route' => $route."/".$fNameExt,
            'image' => $route."/".$fName.".".$imgExtension,
            'form_id' => $form->id
        ]);
        return redirect(route('dashboard'))->with('success', 'Diseño añadido con éxito!');
    }
    public function delete($id) {
        $template = Template::find($id);
        $template->delete();
        return redirect(route('dashboard'))->with('success', 'Diseño eliminado con éxito!');
    }
}
