<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\FormNewRequest;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use App\Models\Form;


class FormsController extends Controller
{
    public function index() {
        $formularios = Form::all();
        return view('admin.formularios.formularios-list', compact('formularios'));
    }
    public function save(FormNewRequest $request) {
        // Almacenar el archivo
        $fileXML = $request->file('fileXML');
        $fileHTML = $request->file('fileHTML');
        $image = $request->file('image');
        $imgExtension = $image->getClientOriginalExtension();
        $fileXMLExtension = $fileXML->getClientOriginalExtension();
        $fileHTMLExtension = $fileHTML->getClientOriginalExtension();
        $fName = $request->input('fileName');
        $route = 'storage/docs/'.$fName;
        $uploadRoute = 'public/docs/'.$fName;
        $xml = $fName.".xml";
        $html = $fName.".html";
        $img = $fName.".".$imgExtension;
        
        //Guardamos los archivos
        $fileXML->storeAs($uploadRoute, $xml);
        $fileHTML->storeAs($uploadRoute, $html);
        $image->storeAs($uploadRoute, $img);
        
        //Creamos el registro en la base de datos
        Form::create([
            'name' => $fName,
            'xml_path' => $route."/".$xml,
            'html_path' => $route."/".$html,
            'image' => $route."/".$img
        ]);
        return redirect(route('dashboard'))->with('success', 'Formulario añadido con éxito!');
    }

    public function delete($id) {
        $formulario = Form::find($id);
        $formulario->delete();
        return redirect(route('dashboard'))->with('success', 'Formulario eliminado con éxito!');
    }
  
}