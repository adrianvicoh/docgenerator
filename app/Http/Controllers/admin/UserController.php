<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class UserController extends Controller
{
    public function index(){
        $users = User::paginate(10);
        return view('admin.users.user-list', compact('users'));
    }

    public function edit(Request $request){
        $validator = Validator::make($request->all(), [
            'id_user' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $id = intval($request->id_user);
        $user = User::find($id);
        $documents = $user->documents()->paginate(10);
        return view('admin.users.user-edit', compact('user', 'documents'));
    }

    public function delete(Request $request){
        $validator = Validator::make($request->all(), [
            'id_user' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $id = intval($request->id_user);
        $user = User::find($id);
        $user->delete();
        return redirect(route('dashboard'))->with('success', 'Usuario eliminado con éxito!');
    }

    public function change(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'nombre' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $id = intval($request->id);
        $user = User::find($id);
        if($request->isAdmin === "1"){
            $user->rol_id = 1;
        }else {
            $user->rol_id = 2;
        }        
        $user->name = $request->nombre;
        $user->save();
        return redirect(route('dashboard'))->with('success', 'Los datos del usuario has sido modificados con éxito!');
    }

    public function getDocs(){
        $user = Auth::user();
        $documentos = $user->documents()->get();
        return view('profile.documents', compact('documentos'));
    }
    /*
                        CÓDIGO POR REVISAR

    public function edit(Request $request){
        $validator = Validator::make($request->all(), [
            'id_usuario' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $id = intval($request->id_usuario);
        $usuario = $usuarios = Usuario::find($id);
        return view('admin.editarUsuario', compact('usuario'));
    }

    public function delete(Request $request){
        $validator = Validator::make($request->all(), [
            'id_usuario' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $id = intval($request->id_usuario);
        $usuario =  Usuario::find($id);
        if($usuario->isAdmin == true){
            return redirect()->route('dashboard')->with('error', 'No se pueden eliminar usuarios administradores');
        }
        $usuario->delete();
        return redirect()->route('dashboard')->with('success', 'El usuario seleccionado se ha eliminado satisfactoriamente!');
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'id_cuenta' => 'required',
            'nombre' => 'required',
            'avatar' => 'required|string|max:255',
            'anoNacimiento' => 'required',
            'isAdmin' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $id = intval($request->id);
        $usuario =  Usuario::find($id);
        $usuario->nombre = $request->nombre;
        $usuario->avatar = $request->avatar;
        $usuario->anoNacimiento = $request->anoNacimiento;
        $usuario->isAdmin = $request->isAdmin;

        $usuario->save();
        return redirect()->route('dashboard')->with('success', 'El usuario seleccionado se ha actualizado satisfactoriamente!');

    }
    */
}