<?php

namespace App\Http\Controllers;

use Attribute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use \Mpdf\Mpdf;

class PruebaController extends Controller
{
    //
    public function cargarPrueba() {
        return view('prueba');
    }

    public function convertPrueba(Request $request) {
        $document = new Mpdf( [
            'mode' => 'utf-8',
            'format' => 'A4',
            'margin_header' => '3',
            'margin_top' => '20',
            'margin_bottom' => '20',
            'margin_footer' => '2',
        ]);
        $xmlFile = file_get_contents(public_path("storage/xml/".$request->docID.".xml"));
        $xml = simplexml_load_string($xmlFile);
        $htmlFile = file_get_contents(public_path("storage/html/".$request->docID.".html"));
        $dom = new \DOMDocument();
        $dom->loadHTML($htmlFile);
        foreach ($xml->children() as $campoGeneral) {
            foreach ($campoGeneral->children() as $campo) {
                $xmlId = (string)$campo['id'];
                $xmlName = (string)$campo['id'];
                foreach ($request->all() as $key => $value) {
                    if ($xmlName === $key) {
                        $newNode = $dom->getElementById($xmlId);
                        $newNode->textContent = $value;
                    }
                }
            }
        }
        $dom->getElementsByTagName('link')[0]->setAttribute('href', "storage/css/".$request->docID.".css");
        $newHtmlFolder = public_path("storage/users/".$request->user."/".$request->docID);
        if (!Storage::exists($newHtmlFolder)) {
            Storage::makeDirectory($newHtmlFolder, 0755, true);
        }
        $newHtmlFile = $newHtmlFolder."/".$request->docID.".html";
        $dom->saveHTMLFile($newHtmlFile);
        $html = $dom->saveHTML();
        $document->WriteHTML($html);
        $filePath = "storage/users/".$request->user."/".$request->docID."/".$request->docID.".pdf";
        $document->Output($filePath, 'F');
        return response()->download($filePath);
    }
}
