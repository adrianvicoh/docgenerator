<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\MainController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Dompdf\Dompdf;
use Mpdf\Mpdf;
use Illuminate\Support\Facades\View;
use App\Models\User;
use App\Models\Document;

class DocumentController extends Controller{
    public function delete($id){
        $document = Document::find($id);
        if ($document) {
            $document->delete();
            redirect(route('user-documents'))->with('success', 'El documento se ha eliminado con éxito');
        } else {
            redirect(route('user-documents'))->with('error', 'El documento no se ha podido eliminar');
        }
    }

    public function download($id){
        $document = Document::find($id);
        if ($document) {
            $htmlFile = $document->path_html;
            $html = file_get_contents($htmlFile);
            $pdf = new Mpdf([
                'mode' => 'utf-8',
                'format' => 'A4',
                'margin_header' => '3',
                'margin_top' => '20',
                'margin_bottom' => '20',
                'margin_footer' => '2',
                'default_charset' => 'UTF-8',
            ]);
            $pdf->WriteHTML($html);
            $filePath = "storage/incoming/document.pdf";
            $pdf->Output($filePath, 'F');
            return response()->download("storage/incoming/document.pdf");
        } else {
            redirect(route('user-documents'))->with('error', 'El documento no se ha podido descargar');
        }
    }
/*
    public function procesar(Request $request){
        //dd($request);
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'apellidos' => 'required',
            'fechaFalta' => 'required|date',
            'fechaFirma' => 'required|date',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data = $request->all();
        
        
        // Genera la vista con los datos
        $view = View::make('escolaResult', ['data' => $data]);
        // Convierte la vista a HTML
        $html = $view->render();
        // Crea una instancia de Dompdf
        $dompdf = new Dompdf();
        // Carga el HTML en Dompdf
        $dompdf->loadHtml($html);
        $dompdf->render();

        // Guarda el PDF en el servidor o envíalo al navegador
        $dompdf->stream('nombre_del_archivo.pdf');
        
        //return view('escolaResult', compact('data'));
    }
*/
    public function procesar(Request $request)
{
    $validator = Validator::make($request->all(), [
        'nombre' => 'required',
        'apellidos' => 'required',
        'fechaFalta' => 'required|date',
        'fechaFirma' => 'required|date',
    ]);

    if ($validator->fails()) {
        return redirect()->back()
            ->withErrors($validator)
            ->withInput();
    }
    $data = $request->all();

    // Crea una instancia de Mpdf
    $mpdf = new Mpdf();

    // Configura Mpdf para cargar los estilos CSS
    $mpdf->WriteHTML(\Mpdf\HTMLParserMode::HEADER_CSS);

    // ...

    // Genera el contenido HTML con los datos
    $html = view('escolaResult', ['data' => $data])->render();

    // Convierte el HTML a PDF
    $mpdf->WriteHTML($html);

    // Guarda el PDF en el servidor o envíalo al navegador
    $mpdf->Output('nombre_del_archivo.pdf', 'D');

}

}
