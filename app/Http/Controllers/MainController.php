<?php

namespace App\Http\Controllers;
use App\Models\Form;
use App\Models\Document;
use App\Models\Template;
use \Mpdf\Mpdf;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;    
use App\Http\Requests\CurriculumRequest;

use Illuminate\Http\Request;

class MainController extends Controller
{
    //

    public function paginaSeleccion() {
        $forms = Form::all();
        return view('pages.seleccion', ['forms' => $forms]);
    }

    public function paginaEstilos($formId) {
        $form = Form::find($formId);
        $estilos = $form->templates;
        return view('pages.estilo', ['estilos' => $estilos, 'formId' => $formId]);
    }

    public function paginaFormulario($formId, $templateId) {
        $form = Form::find($formId);
        //dd("forms.".$form->name);
        return view("forms.".$form->name, ['formId' => $formId, 'templateId' => $templateId]);
    }

    public function finalizarAlbaran(Request $request) {
        $validatedData = $request->validate([
            'invoiceNumber' => 'required|numeric',
            'invoiceDate' => 'required|date',
            'productName' => 'required',
            'productQuantity' => 'required|numeric',
            'productUnitPrice' => 'required|numeric',
            'discount' => 'nullable|numeric',
            'invoiceDescription' => 'required',
            'customerName' => 'required',
            'customerSurname' => 'required',
            'customerEmail' => 'required|email',
            'customerPhone' => 'required',
        ]);
        $file = $this->createPDF($request);
        return view('pages.finalizacion', ['file' => $file]);
    }
    public function finalizar(CurriculumRequest $request) {
        if(is_null($request->language1)){
            $request->merge([
                'language1' => '',
                'level1' => '',
            ]);
        }
        if(is_null($request->language2)){
            $request->merge([
                'language2' => '',
                'level2' => '',
            ]);
        }
        if(is_null($request->language3)){
            $request->merge([
                'language3' => '',
                'level3' => '',
            ]);
        }
        $file = $this->createPDF($request);
        return view('pages.finalizacion', ['file' => $file]);
    }

    public function descargar(/*$file*/) {
        return response()->download("storage/incoming/document.pdf");
    }
    
    // Función para crear el PDF
    public function createPDF($data) {
        $template = Template::find($data->templateId);
        $document = new Mpdf([
            'mode' => 'utf-8',
            'format' => 'A4',
            'margin_header' => '3',
            'margin_top' => '20',
            'margin_bottom' => '20',
            'margin_footer' => '2',
            'default_charset' => 'UTF-8',
        ]);
        $xmlFile = file_get_contents(public_path($template->form->xml_path));
        $xml = simplexml_load_string($xmlFile);
        $htmlFile = file_get_contents(public_path($template->form->html_path));
        $htmlFile = mb_convert_encoding($htmlFile, 'UTF-8', 'UTF-8');
        $dom = new \DOMDocument();
        $dom->loadHTML($htmlFile);
        foreach ($xml->children() as $campoGeneral) {
            foreach ($campoGeneral->children() as $campo) {
                $xmlId = (string)$campo['id'];
                $xmlName = (string)$campo['id'];
                foreach ($data->all() as $key => $value) {
                    if ($xmlName === $key) {
                        $newNode = $dom->getElementById($xmlId);
                        $newNode->textContent = $value;
                        $campo = $xml->xpath("//campo[@id='$xmlId']");
                        $campo[0][0] = strval($value);
                    }
                }
            }
        }
        $dom->getElementsByTagName('link')[0]->setAttribute('href', $template->route);
        if(Auth::check()) {
            $newDocument = new Document();
            $newDocument->user_id = Auth::user()->id;
            $newDocument->template_id = $data->templateId;
            $newDocument->form_id = $data->formId;
            $newDocument->save();
            $newDocument->name = "doc".Auth::user()->id.$newDocument->id;
            $newDocument->save();
            $directory = "storage/users/".Auth::user()->id."/".$newDocument->id;
            if (!File::exists($directory)) {
                File::makeDirectory($directory, 0755, true);
            }
            $newDocument->path_html = $directory."/".$newDocument->name.".html";
            $newDocument->save();
            $dom->saveHTMLFile($newDocument->path_html);
            $html = $dom->saveHTML();
            $document->WriteHTML($html);
            $xmlPath = $directory."/".$newDocument->name.".xml";
            $newDocument->path_xml = $xmlPath;
            //file_put_contents($xmlPath, $xml->asXML());
            $xml->asXML($xmlPath);
            $filePath = "storage/incoming/document.pdf";
            $document->Output($filePath, 'F');
            return $filePath;
        } else {
            $dom->saveHTMLFile("storage/incoming/document.html");
            $html = $dom->saveHTML();
            $document->WriteHTML($html);
            $filePath = "storage/incoming/document.pdf";
            $document->Output($filePath, 'F');
            return $filePath;
        }
    }
}
