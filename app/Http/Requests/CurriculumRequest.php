<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CurriculumRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'surname' => 'required',
            'birthday' => 'required|date',
            'email' => 'required|email',
            'address' => 'required',
            'telephone' => 'required',
            'company' => 'required',
            'position' => 'required',
            'startDate' => 'required|date',
            'endDate' => 'required|date',
            'workDetails' => 'required',
            'school' => 'required',
            'degree' => 'required',
            'startDateDegree' => 'required|date',
            'endDateDegree' => 'required|date',
            'degreeDetails' => 'required',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if (
                empty($this->input('language1')) &&
                empty($this->input('language2')) &&
                empty($this->input('language3'))
            ) {
                $this->merge([
                    'language1' => 'No hay datos',
                    'language2' => 'No hay datos',
                    'language3' => 'No hay datos',
                ]);
            }
        });
    }

    public function messages()
    {
        return [
            'name.required' => 'El campo Nombre es obligatorio.',
            'surname.required' => 'El campo Apellidos es obligatorio.',
            'birthday.required' => 'El campo Fecha de nacimiento es obligatorio.',
            'birthday.date' => 'El campo Fecha de nacimiento debe ser una fecha válida.',
            'email.required' => 'El campo Correo electrónico es obligatorio.',
            'email.email' => 'El campo Correo electrónico debe ser una dirección de correo válida.',
            'address.required' => 'El campo Dirección es obligatorio.',
            'telephone.required' => 'El campo Teléfono es obligatorio.',
            'company.required' => 'El campo Nombre de la empresa es obligatorio.',
            'position.required' => 'El campo Cargo en la empresa es obligatorio.',
            'startDate.required' => 'El campo Fecha de inicio es obligatorio.',
            'startDate.date' => 'El campo Fecha de inicio debe ser una fecha válida.',
            'endDate.required' => 'El campo Fecha de finalización es obligatorio.',
            'endDate.date' => 'El campo Fecha de finalización debe ser una fecha válida.',
            'workDetails.required' => 'El campo Detalles laborales es obligatorio.',
            'school.required' => 'El campo Centro educativo es obligatorio.',
            'degree.required' => 'El campo Nombre de la formación es obligatorio.',
            'startDateDegree.required' => 'El campo Fecha de inicio es obligatorio.',
            'startDateDegree.date' => 'El campo Fecha de inicio debe ser una fecha válida.',
            'endDateDegree.required' => 'El campo Fecha de finalización es obligatorio.',
            'endDateDegree.date' => 'El campo Fecha de finalización debe ser una fecha válida.',
            'degreeDetails.required' => 'El campo Detalles académicos es obligatorio.',
        ];
    }
}
