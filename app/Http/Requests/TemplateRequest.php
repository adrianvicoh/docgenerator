<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TemplateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
    public function withValidator($validator){
        $validator->after(function ($validator) {
            // Validar la extensión del archivo
            $file = $this->file('file');
            $extension = $file->getClientOriginalExtension();

            if ($extension !== 'css') {
                $validator->errors()->add('file', 'El archivo debe ser de tipo css.');
            }
        });
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'fileName' => 'required',
            'file' => 'required|file',
            'image' => 'required|mimes:jpeg,jpg,png',
            'tipoArchivo' => 'required'
        ];
    }
}
