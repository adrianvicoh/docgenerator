<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Form;

class FormNewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    public function withValidator($validator){
        $validator->after(function ($validator) {
            // Validar la extensión del archivo
            $fileXML = $this->file('fileXML');
            $fileHTML = $this->file('fileHTML');
            $extensionXML = $fileXML->getClientOriginalExtension();
            $extensionHTML = $fileHTML->getClientOriginalExtension();

            if ($extensionXML !== 'xml') {
                $validator->errors()->add('fileXML', 'El archivo XML no es del tipo correcto');
            }
            if ($extensionHTML !== 'html') {
                $validator->errors()->add('fileHTML', 'El archivo HTML no es del tipo correcto');
            }
        });
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'fileName' => [
                'required',
                function ($attribute, $value, $fail) {
                    $existingForm = Form::where('name', $value)->first();
                    if ($existingForm) {
                        $fail('El nombre de archivo ya existe en la tabla de formularios.');
                    }
                },
            ],
            'fileXML' => 'required',
            'fileHTML' => 'required',
            'image' => 'required|mimes:jpeg,jpg,png'
        ];
    }
}
