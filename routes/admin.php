<?php


use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\RegisteredUserController;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Middleware\AdminAccess;
use App\Http\Controllers\admin\UserController;
use App\Http\Controllers\admin\TemplateController;
use App\Http\Controllers\admin\FormsController;


Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('eliminarCuenta');
});

Route::middleware(['web', 'auth', 'admin_access'])->prefix('admin')->group(function () {
    Route::get('/dashboard', function () { return view('dashboard'); })->name('dashboard');
    Route::post('/dashboard', function () { return view('dashboard'); })->name('dashboard');
    
    // USUARIOS
    Route::get('/usuarios', [UserController::class, 'index'])->name('user-list');
    Route::post('/usuario/editar/form', [UserController::class, 'edit'])->name('user-edit-form');
    Route::post('/usuario/editar/save', [UserController::class, 'change'])->name('user-edit-save');
    Route::get('/usuario/editar/form', function () { return view('admin.users.user-edit');})->name('user-edit-form');
    Route::get('/usuario/editar/save', function () { return view('dashboard');})->name('user-edit-save');
    Route::post('/usuario/delete', [UserController::class, 'delete'])->name('user-delete-form');
    
    // TEMPLATES
    Route::get('/templates', [TemplateController::class, 'index'])->name('template-list');
    Route::post('/templates/add', [TemplateController::class, 'add'])->name('template-add');
    Route::get('/template/delete/{id_template}', [TemplateController::class, 'delete'])->name('template-delete');
    Route::middleware('FormsMiddleware')->group(function () {
        Route::get('/templates/new', function (Request $request) { 
            $forms =  $request->input('forms');
            return view('admin.templates.template-new')->with('forms', $forms);
        })->name('template-new');
    });

    // FORMULARIOS
    Route::get('/formularios', [FormsController::class, 'index'])->name('forms-list');
    Route::get('/form/delete/{id_formulario}', [FormsController::class, 'delete'])->name('forms-delete');
    Route::get('/formulario/add', function() {
        return view('admin.formularios.formulario-new');
    })->name('forms-add');
    Route::post('/formulario/save', [FormsController::class, 'save'])->name('forms-save');
});

