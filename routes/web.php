<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PruebaController;
use App\Http\Controllers\admin\UserController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\MainController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [MainController::class, 'paginaSeleccion']);
Route::get('/escola', function () {
    return view('escola');
});
Route::post('/escola/procesar', [DocumentController::class, 'procesar'])->name('procesar');
Route::get('/estilos/{id}', [MainController::class, 'paginaEstilos'])->name('estilos');

Route::get('/formulario/{formId}/{templateId}', [MainController::class, 'paginaFormulario'])->name('formulario');

Route::post('/finalizar', [MainController::class, 'finalizar'])->name('finalizar');
Route::post('/finalizarAlbaran', [MainController::class, 'finalizarAlbaran'])->name('finalizarAlbaran');

//Route::get('/descargar/{file}', [MainController::class, 'descargar'])->name('descargar');

Route::get('/descargar', [MainController::class, 'descargar'])->name('descargar');

/* RUTA DE HOME DE USUARIO CON AUTENTICACIÓN
Route::get('/user/home', function () {
    return view('profile.home');
})->middleware(['auth', 'verified'])->name('home-user');
*/
Route::get('/user/home', function () {
    return view('profile.home');
})->name('user-home');

Route::get('/editar', function () {
    return view('profile.editar-perfil');
});
Route::get('/user/documents', [UserController::class, 'getDocs'])->name('user-documents');

Route::get('/user/security', function () {
    return view('profile.seguridad');
})->name('user-security');

Route::get('/user/security/reset-pass', function () {
    return view('profile.seguridad');
})->name('resetPassword');

Route::get('/document/delete/{document_id}', [DocumentController::class, 'delete']);
Route::get('/document/download/{document_id}', [DocumentController::class, 'download']);

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});


Route::get('/user/security', function () {
    return view('profile.seguridad');
})->name('user-security');


Route::get('/prueba/form', [PruebaController::class, 'cargarPrueba']);
Route::post('/prueba/send', [PruebaController::class, 'convertPrueba']);


require __DIR__.'/auth.php';
require __DIR__.'/admin.php';

