# 1. Idea de proyecto: generador de documentos

Este proyecto, realizado por Pedro Muñoz Reynés y Adrián Vico Hernández, consiste en una página web que genere distintos tipos de documentos PDF en el que el usuario pueda elegir una plantilla y rellenar datos en un formulario.

Generaría documentos como currículums, formularios, facturas, albaranes, etc.

La idea buscaría ofrecer una solución fácil para quien quiera generar estos documentos y no tenga grandes conocimientos de informática.

Se ha escogido esta idea como un reto para simplificar otras herramientas similares a esta y como un impulso para aprender más sobre frameworks con modelo vista controlador.

# 2. Modelo conceptual

Para planificar y diseñar el proyecto, tomando como referencia algunas aplicaciones móviles y web que generan currículums, se ha decidido dar un proceso al usuario más simple, basado en pantallas: al acceder al índex, en la primera pantalla, el usuario selecciona el tipo de documento, en la siguiente el diseño, a continuación pasa a la de completar los campos para ese tipo, y en la ultima descarga el archivo PDF generado.

El usuario puede registrarse e iniciar sesión, con lo que se guardará el documento creado en su cuenta, que podrá borrar y editar. Los administradores pueden crear nuevos tipos de documentos y diseños, así como listar y eliminar usuarios.

## 2.0. Diagrama de casos de uso

![](./documents/01_casos_usos.drawio.png) 

# 3. Modelo lógico relacional

El esquema de base de datos ha sido establecido para que cada tipo de documento esté definido por una entidad Form, que contiene las rutas a un XML que define los datos que debe contener y un HTML con la estructura de documento para esos datos. Cada diseño de documento está definida por la entidad Template, con un ruta a un CSS. Entre Form y Template hay una relación de muchos a uno, en tanto que un tipo de documento tendrá diferentes diseños que sólo pertenecen a uno.

Cada usuario tiene también una entidad User, y tiene una relación con la entidad Rol. Cada documento que genere tendría una entidad Document, de manera que un usuario tendrá varios documentos y cada uno pertenece a un solo usuario. Cada documento tiene asociado un Form y un Template. Así mismo, cada Document tendrá una ruta al XML con los datos guardados por el usuario y otra al respectivo HTML.

## 3.0. Esquema conceptual

![](./documents/base_datos.png)

## 3.1. Esquema lógico

- usuario(**idUsuario**, nickname, nombre, apellido, avatar, password, email, idDoc, idRol)
- documento(**idDoc**, nombre, ruta, timestamp, idPlantilla)
- plantilla(**idPlantilla**, ruta, timestamp)
- formulario(**idPlantilla**, nombre, ruta, timestamp, idDocumento)

## 3.2. Diagrama referencial

| Relació referencial | Clau aliena | Relació referida |
| ------------------- | ----------- | ---------------- |
| usuario             | idDoc       | documento        |
| usuario             | idRol       | rol              |
| documento           | idPlantilla | plantilla        |
| formulario          | idDocumento | documento        |

## 3.3. Modelo físico: SQL

```sql
-- Crear la tabla de Rol
CREATE TABLE Rol (
id INT PRIMARY KEY,
rol VARCHAR(255)
);

-- Crear la tabla de Documento
CREATE TABLE Documento (
id INT PRIMARY KEY,
nombre VARCHAR(255),
ruta VARCHAR(255),
timestmp VARCHAR(255),
idPlantilla INT,
FOREIGN KEY (idPlantilla) REFERENCES Plantilla(id)
);

-- Crear la tabla de Usuario
CREATE TABLE Usuario (
id INT PRIMARY KEY,
nickname VARCHAR(255),
nombre VARCHAR(255),
apellido VARCHAR(255),
passwd VARCHAR(255),
avatar VARCHAR(255),
email VARCHAR(255),
idDoc INT,
idRol INT,
FOREIGN KEY (idDoc) REFERENCES Documento(id),
FOREIGN KEY (idRol) REFERENCES Rol(id)
);

-- Crear la tabla de Formulario
CREATE TABLE Formulario (
id INT PRIMARY KEY,
nombre VARCHAR(255),
ruta VARCHAR(255),
idDocumento INT,
timestmp VARCHAR(255),
FOREIGN KEY (idDocumento) REFERENCES Documento(id)
);

-- Crear la tabla de Plantilla
CREATE TABLE Plantilla (
id INT PRIMARY KEY,
ruta VARCHAR(255),
timestmp VARCHAR(255)
);
```

# 4. Planificación

Hemos divido el plan de desarrollo en cuatro Sprints:

- Diseño de las vistas.

- Desarrollo de los modelos y la base de datos.

- Investigación de librerías y desarrollo de controladores.

- Forma final y solución de incidencias.

# 5. Tecnologías utilizadas

- **Laravel**: es un framework con estructura de modelo vista controlador basado en PHP. Esta tecnología nos permite hacer uso de ese lenguaje para poder programar las funciones en un entorno de servidor e instalar las librerías necesarias, además de hacer una buena gestión de la base de datos y los archivos.

- **Mpdf**: librería de PHP y Laravel que permite convertir archivos HTML en PDF en base a ciertos parámetros.

- **DOMdocument**: librería de PHP para manipular y modificar archivos HTML.

- **SimpleXML**: librería de PHP para trabajar con archivos XML.

# 6. Diseño del proyecto

Página de Índice y selector de tipo de documento

![](./documents/03_1.png)

Selector de diseño de documento

![](./documents/03_2.png)

Formulario para introducir los contenidos

![](./documents/03_3.png)

Página de finalización del proceso

![](./documents/03_4.png)

Página de finalización para usuarios registrados

![](./documents/03_5.png)

Página de inicio de sesión

![](./documents/03_6.png)

Página de registro

![](./documents/03_7.png)

Página personal de usuario

![](./documents/03_8.png)

Página de usuario para gestionar documentos

![](./documents/03_9.png)

Página de seguridad de usuarios

![](./documents/03_91.png)

# 7. Desarrollo final, problemas y mejoras

El resultado final del proyecto ha sido satisfactorio en la medida que las funciones principales planificadas se pueden llevar acabo:

- Selección de tipo de documento.

- Selección de diseño.

- introducción de datos.

- Descarga de documento.

- Creación de usuarios, documentos, tipos de documento y diseños.

Las funcionalidades que faltan, y que se quieren plantear para una futura versión son:

- Edición y eliminación de documentos, aunque la vista ya lo prevé, falta el controlador que se encargue de ello.

Los problemas que hemos tenido durante el desarrollo son los siguientes:

- Mientras la idea original era que para cada tipo de documento se cargara una vista de formulario propia a partir del archivo XML, llevar acabo este procesamiento requería hacer uso de una librería de la que falta documentación y requiere usar una API de Java, que nos llevaba mucho tiempo desarrollar. En su lugar, hemos establecido hacer una vista para cada formulario. El problema de ello es que se tendría que subir el archivo de vista además del XML por cada nuevo que se cree, eso implica tener que acceder a la carpeta Resources, que no es accesible.

- Para poder hacer el controlador para transformar el HTML en PDF, hemos usado inicialmente un archivo binario de Wkhtmltopdf, con un controlador de Laravel, pero debido a problemas de versiones, se optó por instalar con Composer la biblioteca Mpdf que funciona de forma directa con PHP.
