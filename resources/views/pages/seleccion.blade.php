@extends('layouts.index')

@section('content')

    <div class="row d-flex justify-content-center m-2 mt-5">
        <div class="col-6">
            <div class="progressBar progress" role="progressbar" aria-label="Basic example" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                <div class="current progress-bar">Tipo de documento</div>
                <div class="progress-bar bg-info">Diseño de documento</div>
                <div class="progress-bar bg-info">Rellenar campos</div>
                <div class="progress-bar bg-info">Descargar documento</div>
            </div>
        </div>
    </div>

    <div id="contenedor-carrusel">
        <h1 class="titulo">Elige el tipo de documento</h1>
        <div class="carousel container">

            @foreach ($forms as $form)
                <div class="slide">
                    <a href="{{ route('estilos', ['id' => $form->id]) }}"><img src="{{ asset($form->image) }}"></a>
                    <br/>
                    <p>{{ $form->name }}</p>
                </div>
            @endforeach

        </div>
    </div>


@endsection