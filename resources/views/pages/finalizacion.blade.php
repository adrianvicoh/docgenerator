@extends('layouts.index')

@php 
    $mostrar = "btn-esconder";
    $message = "El documento ha sido generado con éxito.<br/>Puedes descargarlo o enviarlo por mail";
    if(Auth::check()) {
        $mostrar = "btn-mostrar";
        $message = "El documento ha sido generado.<br/>Puedes descargarlo, guardarlo<br/>o enviarlo por mail";
    }
@endphp

@section('content')

    <div class="row d-flex justify-content-center m-2 mt-5">
        <div class="col-6">
            <div class="progressBar progress" role="progressbar" aria-label="Basic example" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                <div class="finished progress-bar"><a href="/">Tipo de documento</a></div>
                <div class="finished progress-bar"><a href="/estilos">Diseño de documento</a></div>
                <div class="finished progress-bar"><a href="/formularios">Rellenar campos</a></div>
                <div class="current progress-bar">Descargar documento</div>
            </div>
        </div>
    </div>

    <h1 class="titulo">{!! $message !!}</h1>
    <div class="botones-descarga">
        <a id="btn-descarga" href="{{ route('descargar', ['file' => $file]) }}" class="btn btn-light">Descargar</a>
        <button id="btn-enviar" type="button" class="btn btn-light">Enviar por e-mail</button>
        <button id="btn-guardar" type="button" class="btn btn-light {{ $mostrar }}">Guardar</button>
    </div>
@endsection