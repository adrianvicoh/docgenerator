@extends('layouts.index')

@section('content')

    <div class="row d-flex justify-content-center m-2 mt-5">
        <div class="col-6">
            <div class="progressBar progress" role="progressbar" aria-label="Basic example" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                <div class="finished progress-bar"><a href="/">Tipo de documento</a></div>
                <div class="finished progress-bar"><a href="/estilos">Diseño de documento</a></div>
                <div class="current progress-bar">Rellenar campos</div>
                <div class="progress-bar bg-info">Descargar documento</div>
            </div>
        </div>
    </div>

    <div class="container-md p-5">
        <form method="GET" action="/finalizacion">

            <!--================Datos personales================-->

            <h4 class="mb-3">Datos Personales</h4>

            <input type="hidden" name="docID" value="prueba">

            <input type="hidden" name="user" value="usuarioPrueba">

            <div class="row">
                <div class="col-6">
                    <div class="form-group m-2">
                        <label for="nameInput">Nombre</label>
                        <input class="form-control" id="nameInput" placeholder="Nombre" name="name">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group m-2">
                        <label for="surnameInput">Apellidos</label>
                        <input class="form-control" id="surnameInput" placeholder="Apellidos" name="surname">
                    </div>
                </div>  
            </div>

            <div class="row">
                <div class="col-6">
                    <div class="form-group m-2">
                        <label for="birthdayInput">Fecha de nacimiento</label>
                        <input type="date" class="form-control" id="birthdayInput" placeholder="Fecha nacimiento" name="birthday">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group m-2">
                        <label for="emailInput">Correo electrónico</label>
                        <input type="email" class="form-control" id="emailInput" placeholder="Correo electrónico" name="email">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <div class="form-group m-2">
                        <label for="addressInput">Dirección</label>
                        <input type="text" class="form-control" id="addressInput" placeholder="Dirección" name="address">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group m-2">
                        <label for="telephoneInput">Teléfono</label>
                        <input type="tel" class="form-control" id="telephoneInput" placeholder="Teléfono" name="telephone">
                    </div>
                </div>
            </div>

            <!--================Experiencia laboral================-->
            
            <h4 class="mb-3 mt-3">Experiencia laboral</h4>

            <div class="row">
                <div class="col-6">
                    <div class="form-group m-2">
                        <label for="companyInput">Nombre de la empresa</label>
                        <input type="text" class="form-control" id="companyInput" placeholder="Nombre de la empresa" name="company">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group m-2">
                        <label for="positionInput">Cargo en la empresa</label>
                        <input type="text" class="form-control" id="positionInput" placeholder="Cargo en el empresa" name="position">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <div class="form-group m-2">
                        <label for="startDateInput">Fecha de inicio</label>
                        <input type="date" class="form-control" id="startDateInput" placeholder="Fecha de inicio" name="startDate">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group m-2">
                        <label for="endDateInput">Fecha de finalización</label>
                        <input type="date" class="form-control" id="endDateInput" placeholder="Fecha de finalización" name="endDate">
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-12">
                    <div class="form-group m-2">
                        <label for="workDetailsInput">Detalles</label>
                        <textarea type="textarea" class="form-control" id="workDetailsInput" rows="4" placeholder="Escribe de forma detalla las actividades realizadas en la empresa" name="workDetails"></textarea>
                    </div>
                </div>
            </div>

            <!--================Formación================-->

            <h4 class="mb-3 mt-3">Formación</h4>

            <div class="row">
                <div class="col-6">
                    <div class="form-group m-2">
                        <label for="schoolInput">Centro educativo</label>
                        <input type="text" class="form-control" id="schoolInput" placeholder="Centro educativo" name="school">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group m-2">
                        <label for="degreeInput">Nombre de la formación</label>
                        <input type="text" class="form-control" id="degreeInput" placeholder="Nombre de la formación" name="degree">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <div class="form-group m-2">
                        <label for="startDateDegreeInput">Fecha de inicio</label>
                        <input type="date" class="form-control" id="startDateDegreeInput" placeholder="Fecha de inicio" name="startDateDegree">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group m-2">
                        <label for="endDateDegreeInput">Fecha de finalización</label>
                        <input type="date" class="form-control" id="endDateDegreeInput" placeholder="Fecha de finalización" name="endDateDegree">
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-12">
                    <div class="form-group m-2">
                        <label for="degreeDetailsInput">Detalles</label>
                        <textarea type="textarea" class="form-control" id="degreeDetailsInput" rows="4" placeholder="Escribe de forma detalla la formación recibida" name="degreeDetails"></textarea>
                    </div>
                </div>
            </div>

            <!--================Información adicional================-->

            <h4 class="mb-3 mt-3 d-flex justify-content-center">Datos adicionales</h4>

            <div class="row d-flex justify-content-center">
                <div class="col-6">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group m-2">
                                <label for="language1Input">Idiomas</label>
                                <input type="text" class="form-control mt-1" id="language1Input" placeholder="Idioma 1" name="language1">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group m-2">
                                <label for="level1Input">Nivel</label>
                                <select class="form-control mt-1" id="level1Input" placeholder="Nivel de Idioma 1" name="level1">
                                    <option value="Básico">Básico</option>
                                    <option value="Medio">Medio</option>
                                    <option value="Avanzado">Avanzado</option>
                                </select>
                            </div>
                        </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group m-2">
                            <input type="text" class="form-control mt-1" id="language2Input" placeholder="Idioma 2" name="language2">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group m-2">
                            <select class="form-control mt-1" id="level2Input" placeholder="Nivel de Idioma 2" name="level2">
                                <option value="Básico">Básico</option>
                                <option value="Medio">Medio</option>
                                <option value="Avanzado">Avanzado</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group m-2">
                            <input type="text" class="form-control mt-1" id="language3Input" placeholder="Idioma 3" name="language3">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group m-2">
                            <select class="form-control mt-1" id="level3Input" placeholder="Nivel de Idioma 3" name="level3">
                                <option value="Básico">Básico</option>
                                <option value="Medio">Medio</option>
                                <option value="Avanzado">Avanzado</option>
                            </select>
                        </div>
                    </div>
                </div>
                </div>
            </div>

            <!--================Botón de envío================-->

            <div class="row mt-3">
                <div class="col-12 d-flex justify-content-center">
                    <button type="submit" class="btn btn-primary">Enviar formulario</button>
                </div>
            </div> 
          </form>
    </div>


@endsection