@extends('layouts.index')

@section('content')

    <div class="row d-flex justify-content-center m-2 mt-5">
        <div class="col-6">
            <div class="progressBar progress" role="progressbar" aria-label="Basic example" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                <div class="finished progress-bar"><a href="/">Tipo de documento</a></div>
                <div class="current progress-bar">Diseño de documento</div>
                <div class="progress-bar bg-info">Rellenar campos</div>
                <div class="progress-bar bg-info">Descargar documento</div>
            </div>
        </div>
    </div>

    <h1 class="titulo">Escoge el diseño del documento</h1>
    <div class="album py-5">
        <div class="container">
            <div class="row">

                @foreach ($estilos as $estilo)
                    <div class="col-md-3 my-card">
                        <div class="card mb-1 shadow-sm h-100">
                            <a href="{{ route('formulario', ['formId' => $formId, 'templateId' => $estilo->id]) }}">
                                <img class="bd-placeholder-img card-img-top" src="{{ asset($estilo->image) }}" role="img" preserveAspectRatio="xMidYMid slice" focusable="false">
                            </a>
                            <div class="card-body my-card">
                                <p class="card-text text-truncate">{{ $estilo->name }}</p>
                            </div>
                        </div>
                    </div> 
                @endforeach
                
            </div>
        </div>
    </div>

@endsection