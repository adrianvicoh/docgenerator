@extends('layouts.index')

@section('content')
<div class="container-fluid">
    
    <div class="row">
    
    <!-- MENÚ LATERAL -->
    <div class="col-2 menu-lateral">
        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#menuLateral" aria-controls="menuLateral" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse d-lg-block" id="menuLateral">
            <div class="menu-content">
                @include('components.menu-lateral')
            </div>
        </div>
     </div>
     <!-- FIN MENÚ LATERAL -->

        <div class="col-10">
            @if(session('success'))
                <div class="alert alert-success my-form-item">{{ session('success') }}</div>
            @endif
            @if(session('status') === 'password-updated')
                <div class="alert alert-success">
                    Contraseña modificada con éxito.
                </div>
            @endif
            @if($errors->updatePassword->any())
            @foreach($errors->updatePassword->all() as $error)
                <div class="alert alert-danger my-form-item">{{ $error }}</div>
                @endforeach
            @endif
            <h1 class="titulo">Cambio de contraseña</h1>
            <div class="m-auto w-40 change-pass">
                <form action="{{ route('password.update') }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="inputContrasena" class="my-text">Contraseña actual:</label>
                        <input type="password" class="form-control" name="currentPassword" id="inputContrasena" placeholder="Contraseña actual">
                    </div>
                    <div class="form-group">
                        <label for="inputNuevaContrasena" class="my-text">Nueva contraseña:</label>
                        <input type="password" class="form-control" name="newPassword" id="inputNuevaContrasena" placeholder="Nueva contraseña">
                    </div>
                    <div class="form-group">
                        <label for="inputConfirmarContrasena" class="my-text">Confirmar nueva contraseña:</label>
                        <input type="password" class="form-control" name="newPassword_confirmation" id="inputConfirmarContrasena" placeholder="Confirmar nueva contraseña">
                    </div>
                    <button type="submit" class="w-100 mt-3 btn btn-success">Cambiar contraseña</button>
                </form>
            </div>

        </div>
            
    </div>
</div>
@endsection
