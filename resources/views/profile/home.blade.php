@extends('layouts.index')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-4 menu-lateral">
            @include('components.menu-lateral')
        </div>
        <div class="col-8 d-flex justify-content-start">
            <div class="row justify-content-md-center">
                <div class="col-12">
                    <h1 class="titulo">Bienvenido, {{ Auth::user()->name }}</h1>
                    <img class="rounded-circle avatar img-fluid" src="{{ asset(Auth::user()->avatar) }}">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
