@extends('layouts.index')

@section('content') 
 
     <div class="container-fluid">
     @if (session('success'))
        <br/><br/>
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @if (session('error'))
        <br/><br/>
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
     <div class="row">
     <div class="col-2 menu-lateral">
        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#menuLateral" aria-controls="menuLateral" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse d-lg-block" id="menuLateral">
            <div class="menu-content">
                @include('components.menu-lateral')
            </div>
        </div>
     </div>

        <div class="col-10 col-sm">
            <h1 class="titulo">Mis Documentos</h1>
            <div class="panel panel-primary custom-width">
                <div class="panel-heading">
                    Lista de documentos
                </div>
                <div class="panel-body">
                    <ul class="list-group">
                    @foreach ($documentos as $documento)
                        <li class="list-group-item">
                            {{ $documento->name }}
                            <div class="pull-right action-buttons">
                                <ul>
                                    <button class="btn btn-primary"><a href="#">Editar</a></button>
                                    <button class="btn btn-success"><a href="/document/download/{{ $documento->id }}">Descargar</a></button>
                                    <button class="btn btn-danger"><a href="/document/delete/{{ $documento->id }}">Eliminar</a></button>
                                </ul>
                            </div>
                        </li>
                    @endforeach
                    </ul>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-md-6">
                            <h6>
                                Nº de documentos <span class="label label-info">{{ count($documentos) }}</span></h6>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
            
    </div>
</div>


@endsection