@extends('layouts.index')

@section('content')
    <div class="container-fluid">
        <div class="row">
            
            <div class="col-4 menu-lateral">
                @include('components.menu-lateral')
            </div>
            <div class="col-7 div-centrado">
                <div class="container editar-perfil">
                    <h1 class="titulo">Usuario</h1>
                </div>
            </div>
        </div>
    </div>
@endsection
