@extends('layouts.index')

@section('content')
    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="alert alert-danger my-form-item">{{ $error }}</div>
        @endforeach
    @endif
    <div class="row d-flex justify-content-center m-2 mt-5">
        <div class="col-6">
            <div class="progressBar progress" role="progressbar" aria-label="Basic example" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                <div class="finished progress-bar"><a href="/">Tipo de documento</a></div>
                <div class="finished progress-bar"><a href="/estilos">Diseño de documento</a></div>
                <div class="current progress-bar">Rellenar campos</div>
                <div class="progress-bar bg-info">Descargar documento</div>
            </div>
        </div>
    </div>

    <div class="container-md p-5">
    <form method="POST" action="{{ route('finalizarAlbaran') }}" enctype="application/x-www-form-urlencoded">
    @csrf

        <input type="hidden" name="formId" value="{{$formId}}">
        <input type="hidden" name="templateId" value="{{$templateId}}">

        <!--================Detalles del albarán================-->
        <h4 class="mb-3">Detalles del Albarán</h4>

        <div class="row">
            <div class="col-6">
                <div class="form-group m-2">
                    <label for="invoiceNumber">N Factura</label>
                    <input type="number" class="form-control" id="invoiceNumber" placeholder="Producto" name="invoiceNumber">
                </div>
            </div>
            <div class="col-6">
                    <div class="form-group m-2">
                        <label for="invoiceDate">Fecha de facturación</label>
                        <input type="date" class="form-control" id="invoiceDate" placeholder="Fecha de inicio" name="invoiceDate">
                    </div>
                </div>
            <div class="col-6">
                <div class="form-group m-2">
                    <label for="productName">Producto</label>
                    <input class="form-control" id="productName" placeholder="Producto" name="productName">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group m-2">
                    <label for="productQuantity">Cantidad</label>
                    <input class="form-control" id="productQuantity" placeholder="Cantidad" name="productQuantity">
                </div>
            </div>  
        </div>

        <div class="row">
            <div class="col-6">
                <div class="form-group m-2">
                    <label for="productUnitPrice">Precio</label>
                    <input type="text" class="form-control" id="productUnitPrice" placeholder="Precio" name="productUnitPrice">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group m-2">
                    <label for="discount">Descuento</label>
                    <input type="text" class="form-control" id="discount" placeholder="Descuento" name="discount">
                </div>
            </div>
        </div>
        <div class="row">
                <div class="col-12">
                    <div class="form-group m-2">
                        <label for="invoiceDescription">Detalles</label>
                        <textarea type="textarea" class="form-control" id="invoiceDescription" rows="4" placeholder="Descripción del producto" name="invoiceDescription"></textarea>
                    </div>
                </div>
            </div>
        <!--================Datos del cliente================-->
        <h4 class="mb-3 mt-3">Datos del Cliente</h4>

        <div class="row">
            <div class="col-6">
                <div class="form-group m-2">
                    <label for="customerName">Nombre</label>
                    <input class="form-control" id="customerName" placeholder="Nombre" name="customerName">
                </div>
                <div class="form-group m-2">
                    <label for="customerSurname">Apellidos</label>
                    <input class="form-control" id="customerSurname" placeholder="Apellidos" name="customerSurname">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <div class="form-group m-2">
                    <label for="customerEmail">Correo electrónico</label>
                    <input type="email" class="form-control" id="customerEmail" placeholder="Correo electrónico" name="customerEmail">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group m-2">
                    <label for="customerAddress">Dirección</label>
                    <input type="text" class="form-control" id="customerAddress" placeholder="Correo electrónico" name="customerAddress">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group m-2">
                    <label for="customerPhone">Teléfono</label>
                    <input type="tel" class="form-control" id="customerPhone" placeholder="Teléfono" name="customerPhone">
                </div>
            </div>
        </div>

        <!--================Botón de envío================-->
        <div class="row mt-3">
            <div class="col-12 d-flex justify-content-center">
                <button type="submit" class="btn btn-primary">Enviar formulario</button>
            </div>
        </div>

        </form>

    </div>

@endsection