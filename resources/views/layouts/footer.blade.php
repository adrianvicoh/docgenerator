 <!-- FOOTER -->

 <span class="mb-3 mb-md-0 my-text social-media">&copy; 2022 Company, Inc</span>
    <div class="logo-footer">
      <div>
        <a class="navbar-brand logo" href="#"><b><span style="color:blue;">Doc</span>Generator</b></a>
      </div>
      
    </div>

    <ul class="nav justify-content-end list-unstyled d-flex social-media">
      <li class="ms-3"><a href="#"><i class="fa fa-facebook white-text"></i></a></li>
      <li class="ms-3"><a href="#"><i class="fa fa-instagram fa-lg white-text"></i></a></li>
      <li class="ms-3"><a href="#"><i class="fa fa-twitter fa-lg white-text"></i></a></li>
    </ul>