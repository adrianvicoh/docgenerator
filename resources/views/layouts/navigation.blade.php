<header>
  <!-- Intro settings -->

  <!-- Background image -->
  <div id="intro-example" class="p-2 text-center cabecera">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-sm navbar-light bg-transparent" aria-label="Third navbar example">
  <div class="container-fluid">
    <a class="navbar-brand logo" href="/"><b><span style="color:blue;">Doc</span>Generator</b></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample03" aria-controls="navbarsExample03" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    @if (!Auth::check())
      <div class="collapse navbar-collapse ms-auto" id="navbarsExample03">
        <ul class="navbar-nav ms-auto mb-2 mb-sm-0">
          <li class="nav-item btn-navegacion">
            <a class="nav-link btn btn-light" aria-current="page" href="{{ route('register') }}">Registrarse</a>
          </li>
          <li class="nav-item btn-navegacion">
            <a class="nav-link btn btn-light" href="{{ route('login') }}">Login</a>
          </li>
        </ul>
      </div>
    @else
      <div class="collapse navbar-collapse ms-auto" id="navbarsExample03">
        <ul class="navbar-nav ms-auto mb-2 mb-sm-0">
          <li class="nav-item">
          <a href="{{ route('user-home') }}">  
          <div class="image-container btn-user">
              <img src="{{ asset(Auth::user()->avatar) }}" class="rounded-circle avatar-nav img-fluid" alt="logo de usuarios">
            <div class="image-caption">
              {{Auth::user()->name}}
            </div>
          </div>
          </a>
        </li>
        </ul>
      </div>
    @endif
  </div>
</nav>

</div>
  <!-- Navbar -->    
  
</header>