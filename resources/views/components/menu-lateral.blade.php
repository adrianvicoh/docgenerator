@include('components.iconos')
<div class="d-flex flex-column flex-shrink-0 p-3 bg-light" style="height: 100vh;" id="menu-lateral">
        <span class="fs-4">{{ Auth::user()->name }}</span>
    <hr>
    <div class="overflow-auto h-100">
        <ul class="nav nav-pills flex-column mb-auto">
            <li class="nav-item">
                <a href="{{ route('user-home') }}" class="nav-link active" aria-current="page">
                    <svg class="bi me-2" width="16" height="16"><use xlink:href="#home"></use></svg>
                    Perfil
                </a>
            </li>
            <li>
                <a href="{{ route('user-documents') }}" class="nav-link link-dark">
                    <svg class="bi me-2" width="16" height="16"><use xlink:href="#speedometer2"></use></svg>
                    Mis Documentos
                </a>
            </li>
            
            <li>
                <a href="{{ route('user-security') }}" class="nav-link link-dark">
                    <svg class="bi me-2" width="16" height="16"><use xlink:href="#speedometer2"></use></svg>
                    Seguridad y Privacidad
                </a>
            </li>
           @if(Auth::user()->rol_id === 1)
            <li>
                <a href="{{ route('dashboard') }}" class="nav-link link-dark">
                    <svg class="bi me-2" width="16" height="16"><use xlink:href="#speedometer2"></use></svg>
                    <b>Panel de administrador</b>
                </a>
            </li>
            @endif
            <li>
            <a href="{{ route('logout') }}" class="nav-link link-dark">
                    <svg class="bi me-2" width="16" height="16"><use xlink:href="#speedometer2"></use></svg>
                    Logout
                </a>
            </li>
        </ul>
    </div>
    <hr>
    <div class="dropdown">
        <a href="#" class="d-flex align-items-center link-dark text-decoration-none dropdown-toggle" id="dropdownUser2" data-bs-toggle="dropdown" aria-expanded="false">
            <img src="{{ asset(Auth::user()->avatar) }}" alt="" width="32" height="32" class="rounded-circle me-2">
            <strong>{{ Auth::user()->name }}</strong>
        </a>
        <ul class="dropdown-menu text-small shadow" aria-labelledby="dropdownUser2">
            <li><a class="dropdown-item" href="/">Nuevo documento</a></li>
            <li><a class="dropdown-item" href="/editar">Editar perfil</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="#">Salir</a></li>
        </ul>
    </div>
</div>
