@extends('adminlte::page')
@section('content')
    <div class="container">
    @if (session('success'))
        <br/><br/>
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="alert alert-danger my-form-item">{{ $error }}</div>
        @endforeach
    @endif
        <h3 >Tipos de documentos</h3>

        <table class="table table-striped">
            <thead>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="{{route('forms-add')}}" class="btn btn-success w-100">Añadir tipo de documento</a></td>
                </tr>
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Imagen</th>
                    <th>Fecha de creación</th>
                    <th>Fecha de modificación</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($formularios as $form)
                    <tr>
                        <td>{{ $form->id }}</td>
                        <td>{{ $form->name }}</td>
                        <td><img src="{{ asset($form->image) }}" width="100px;"/></td>
                        <td>{{ $form->created_at }}</td>
                        <td>{{ $form->updated_at }}</td>
                        <td>
                        <div class="pull-right action-buttons">
                                <ul>
                                    <form id="deleteForm" action="form/delete/{{ $form->id }}" method="get">
                                        @csrf
                                        <button type="submit" class="btn btn-danger">Eliminar</button>
                                    </form>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection