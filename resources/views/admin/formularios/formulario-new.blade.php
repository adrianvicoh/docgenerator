@extends('adminlte::page')
@section('content')
<br/>
<br/>
<br/>
    <div class="container my-content-box">  
    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="alert alert-danger my-form-item">{{ $error }}</div>
    @endforeach
    @endif  
        <form action="{{ route('forms-save') }}" method="post" enctype="multipart/form-data">
        @csrf   
            <div class="form-group">
                <label for="nombre">Nombre del tipo de documento</label>
                <input type="text" class="form-control" id="fileName" name="fileName" value="{{ old('nombre', '') }}" placeholder="Nombre del archivo">
            </div>
            <div class="form-group mb-3">
                <label for="file" class="form-label"><b>Elige la imagen...</b></label>
                <input class="form-control" type="file" id="image" name="image">
            </div>
            <div class="form-group mb-3">
                <label for="file" class="form-label"><b>Elige tu archivo XML...</b></label>
                <input class="form-control" type="file" id="fileXML" name="fileXML">
            </div>
            <div class="form-group mb-3">
                <label for="file" class="form-label"><b>Elige tu archivo HTML...</b></label>
                <input class="form-control" type="file" id="fileHTML" name="fileHTML">
            </div>
            <button type="submit" class="btn btn-primary">Guardar formulario</button>
        </form>
    </div>
    @vite(['resources/scss/app.scss', 'resources/js/app.js'])
@endsection