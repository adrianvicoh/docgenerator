@extends('adminlte::page')
@section('content')
    <div class="container">
    @if (session('success'))
        <br/><br/>
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="alert alert-danger my-form-item">{{ $error }}</div>
        @endforeach
    @endif
    <br/>
    <br/>
    <br/>
    <div class="container my-content-box">    
        <form action="{{ route('user-edit-save') }}" method="post">
        @csrf   
            <input type="number" class="form-control" id="id" name="id" value="{{ old('id', isset($user) ? $user->id : '') }}" readonly hidden>  
            <div class="form-group">
                <label for="nombre">Nombre</label>
                <input type="text" class="form-control" id="nombre" name="nombre" value="{{ old('nombre', isset($user) ? $user->name : '') }}" placeholder="Nombre del usuario">
            </div>
            <div class="form-group">
                <label for="isAdmin">Es admin:</label>
                <div class="switch">
                    <input type="hidden" name="isAdmin" value="0">
                    <input type="checkbox" id="adminSwitch" name="isAdmin" value="1" {{ old('isAdmin', isset($user) && $user->rol_id == 1 ? 'checked' : '') }}>
                    <label for="adminSwitch"></label>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Guardar cambios</button>
        </form>
    </div>

        <h3>Documentos de {{ $user->name }}</h3>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Nombre del documento</th>
                    <th>Fecha de creación</th>
                    <th>Fecha de la última modificación</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($documents as $document)
                    <tr>
                        <td>{{ $document->name }}</td>
                        <td>{{ $document->created_at }}</td>
                        <td>{{ $document->updated_at }}</td>
                        <td>
                            <form action="#" method="post">
                                @csrf
                                <input type="number" name="id_user" value="{{ $document->id }}" hidden/>
                                <button class="btn btn-danger" type="submit">Eliminar</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <nav aria-label="Navegación de users">
            <ul class="pagination justify-content-center">
                @if($documents->currentPage() > 1)
                    <li class="page-item"><a class="page-link" href="{{ $documents->previousPageUrl() }}" tabindex="-1">Anterior</a></li>
                @endif

                @for ($i = 1; $i <= $documents->lastPage(); $i++)
                    <li class="page-item {{ ($documents->currentPage() == $i) ? 'active' : '' }}"><a class="page-link" href="{{ $documents->url($i) }}">{{ $i }}</a></li>
                @endfor

                @if($documents->currentPage() < $documents->lastPage())
                    <li class="page-item"><a class="page-link" href="{{ $documents->nextPageUrl() }}">Siguiente</a></li>
                @endif
            </ul>
        </nav>
    </div>
    @vite(['resources/scss/app.scss', 'resources/js/app.js'])
@endsection