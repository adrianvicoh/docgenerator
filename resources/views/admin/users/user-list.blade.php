@extends('adminlte::page')
@section('content')
    <div class="container">
    @if (session('success'))
        <br/><br/>
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="alert alert-danger my-form-item">{{ $error }}</div>
        @endforeach
    @endif
        <h3 >Usuarios</h3>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Rol</th>
                    <th>Email</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->rol_id == 1 ? 'Admin' : 'Usuario' }}</td>
                        <td>
                        <div class="pull-right action-buttons">
                                <ul>
                                    <form id="formEditUser" action="" method="post">
                                        <input id="id_user" name="id_user" value="" hidden>
                                        @csrf
                                    </form>
                                    <button class="btn btn-success" onclick="action('editar', {{ $user->id }})">Editar</button>
                                    <button class="btn btn-danger" onclick="action('eliminar', {{ $user->id }})">Eliminar</button>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <nav aria-label="Navegación de users">
            <ul class="pagination justify-content-center">
                @if($users->currentPage() > 1)
                    <li class="page-item"><a class="page-link" href="{{ $users->previousPageUrl() }}" tabindex="-1">Anterior</a></li>
                @endif

                @for ($i = 1; $i <= $users->lastPage(); $i++)
                    <li class="page-item {{ ($users->currentPage() == $i) ? 'active' : '' }}"><a class="page-link" href="{{ $users->url($i) }}">{{ $i }}</a></li>
                @endfor

                @if($users->currentPage() < $users->lastPage())
                    <li class="page-item"><a class="page-link" href="{{ $users->nextPageUrl() }}">Siguiente</a></li>
                @endif
            </ul>
        </nav>
    </div>
    <script type="text/javascript">
        function action(accion, user){
            var editar = "{{ route('user-edit-form') }}";
            var eliminar = "{{ route('user-delete-form') }}";
            event.preventDefault();
            const formulario = document.getElementById("formEditUser");
            const usuario = document.getElementById("id_user");
            usuario.value = user;
            if(accion === 'editar'){
                formulario.action = editar;
            }else if(accion === 'eliminar'){
                formulario.action = eliminar;
            }
            
            console.log(formulario.action);
            formulario.submit();
    
        }
    </script>
@endsection