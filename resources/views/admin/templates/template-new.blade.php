@extends('adminlte::page')
@section('content')
<br/>
<br/>
<br/>
    <div class="container my-content-box">  
    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="alert alert-danger my-form-item">{{ $error }}</div>
    @endforeach
    @endif  
        <form action="{{ route('template-add') }}" method="post" enctype="multipart/form-data">
        @csrf   
            <div class="form-group">
                <label for="nombre">Nombre del diseño</label>
                <input type="text" class="form-control" id="fileName" name="fileName" value="{{ old('nombre', '') }}" placeholder="Nombre del archivo">
            </div>
            <div class="form-group">
                        <label for="idTipoArchivo">Tipo de archivo</label>
                            <select class="form-select" id="idTipoArchivo" name="tipoArchivo">
                                @foreach($forms as $form)
                                    <option value="{{ $form->name }}">{{ $form->name }}</option>
                                @endforeach
                            </select>
                        </div>
            <div class="form-group mb-3">
                <label for="file" class="form-label"><b>Elige la imagen...</b></label>
                <input class="form-control" type="file" id="image" name="image">
            </div>
            <div class="form-group mb-3">
                <label for="file" class="form-label"><b>Elige tu archivo...</b></label>
                <input class="form-control" type="file" id="file" name="file">
            </div>
            <button type="submit" class="btn btn-primary">Guardar diseño</button>
        </form>
    </div>
    @vite(['resources/scss/app.scss', 'resources/js/app.js'])
@endsection