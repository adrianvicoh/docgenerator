@extends('adminlte::page')
@section('content')
    <div class="container">
    @if (session('success'))
        <br/><br/>
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="alert alert-danger my-form-item">{{ $error }}</div>
        @endforeach
    @endif
        <h3 >Usuarios</h3>

        <table class="table table-striped">
            <thead>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><a href="{{ route('template-new') }}" class="btn btn-success w-100">Añadir diseño</a></td>
                </tr>
                <tr>
                    <th>Nombre</th>
                    <th>Imagen</th>
                    <th>Fecha de creación</th>
                    <th>Fecha de modificación</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($templates as $template)
                    <tr>
                        <td>{{ $template->name }}</td>
                        <td><img src="{{ asset($template->image) }}" width="100px;"/></td>
                        <td>{{ $template->created_at }}</td>
                        <td>{{ $template->updated_at }}</td>
                        <td>
                        <div class="pull-right action-buttons">
                                <ul>
                                    <form id="formEditTemplate" action="template/delete/{{ $template->id }}" method="get">
                                        @csrf
                                        <button class="btn btn-danger">Eliminar</button>
                                    </form>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection