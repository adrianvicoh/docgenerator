
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Escola del treball</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

        <!-- Scripts -->
        @vite(['resources/scss/app.scss', 'resources/js/app.js'])
    </head>
    <body class="font-sans antialiased">
        <header>
            <div id="intro-example" class="p-2 text-center cabeceraEdt">
                <img src="/imagenes/escola-del-treball2.png"/>
            </div>
        </header>
    
        <main>
                    <div class="container">
                <h1>Formulario de incidencias</h1>

                <p>Nom: {{ isset($data['nombre']) ? $data['nombre'] : '' }}<br/>
                Cognoms: {{ isset($data['apellidos']) ? $data['apellidos'] : '' }}
                </p>
                <p>Als efectes de 
                    <input type="checkbox" id="checkboxNotificar"{{ isset($data['checkboxNotificar']) && $data['checkboxNotificar'] ? ' checked' : '' }} disabled>
                    notificar i/o 
                    <input type="checkbox" id="checkboxJustificar"{{ isset($data['checkboxJustificar']) && $data['checkboxJustificar'] ? ' checked' : '' }} disabled>
                    justificar la incidència que he tingut en el registre de control de compliment de la meva jornada laboral del dia 
                    <span><b>{{ isset($data['fechaFalta']) ? $data['fechaFalta'] : '' }}</b></span><br>
                    Declaro:
                </p>

                <input type="checkbox" id="incidencia1" {{ isset($data['incidencia1']) && $data['incidencia1'] ? ' checked ' : '' }}disabled>
                <label for="incidencia1">Que m’he oblidat de marcar a l’hora que corresponia i ho he fet més tard.</label><br/>

                <input type="checkbox" id="incidencia2" {{ isset($data['incidencia2']) && $data['incidencia2'] ? ' checked ' : '' }}disabled>
                <label for="incidencia2">Que m’he oblidat de marcar a l’hora que corresponia en iniciar la jornada de treball.</label><br/>

                <input type="checkbox" id="incidencia3" {{ isset($data['incidencia3']) && $data['incidencia3'] ? ' checked ' : '' }}disabled>
                <label for="incidencia3">Que m’he oblidat de marcar a l’hora que corresponia, en finalitzar la meva jornada laboral.</label><br/>

                <input type="checkbox" id="incidencia4" {{ isset($data['incidencia4']) && $data['incidencia4'] ? ' checked ' : '' }}disabled>
                <label for="incidencia4">Que, en tenir una activitat complementària (junta avaluació, d’equip docent, reunió, etc.), no la he tingut en compte i no he marcat quan corresponia.</label><br>

                <input type="checkbox" id="incidencia5" {{ isset($data['incidencia5']) && $data['incidencia5'] ? ' checked ' : '' }}disabled>
                <label for="incidencia5">Que no he marcat per anar a visitar empreses (FPCT).</label><br/>

                <input type="checkbox" id="incidencia6" {{ isset($data['incidencia6']) && $data['incidencia6'] ? ' checked ' : '' }}disabled>
                <label for="incidencia6">Que no he marcat per realitzar les activitats escolars fora del Centre.</label><br/>

                <input type="checkbox" id="incidencia7" {{ isset($data['incidencia7']) && $data['incidencia7'] ? ' checked ' : '' }}disabled>
                <label for="incidencia7">Que m’he incorporat amb retard al Centre per les següents circumstàncies:</label><br/><br/>
                <p><i>{{ isset($data['retardo']) ? $data['retardo'] : '' }}</i></p>
                

                <input type="checkbox" id="incidencia8" {{ isset($data['incidencia8']) && $data['incidencia8'] ? ' checked ' : '' }}disabled>
                <label for="incidencia8">Que he sortit del Centre abans de finalitzar la meva jornada laboral per les següents circumstàncies:</label><br/><br/>
                <p><i>{{ isset($data['salida']) ? $data['salida'] : '' }}</i></p>

                <input type="checkbox" id="incidencia9" {{ isset($data['incidencia9']) && $data['incidencia9'] ? ' checked ' : '' }}disabled>
                <label for="incidencia9">Que no he marcat per les següents circumstàncies:</label><br/><br/>
                <p><i>{{ isset($data['otras']) ? $data['otras'] : '' }}</i></p>

                <input type="checkbox" id="incidencia10" {{ isset($data['incidencia10']) && $data['incidencia10'] ? ' checked ' : '' }}disabled>
                <label for="incidencia10">Que aporto el/s següent/s document/s justificatius:</label><br/><br/>
                <p><i>{{ isset($data['documentos']) ? $data['documentos'] : '' }}</i></p>
                
                <label for="firma">Signatura:</label>
                <div class="firma"></div>
                <p>Barcelona, a <span><b>{{ isset($data['fechaFirma']) ? $data['fechaFirma'] : '' }}<b></span></p>
                <br>
                <br>
                    <p>SR.. DIRECTOR DE L’INSTITUT ESCOLA DEL TREBALL DE BARCELONA</p>

                    <p>Nota: Assenyaleu amb “X” la casella corresponent. La declaració s’ha de presentar a Prefectura d’Estudis el mateix dia de la incidència o sinó l’endemà, si la incidència és a la sortida.</p>
            </div>
        </main>
        <footer class="d-flex flex-wrap justify-content-between align-items-center py-3 bg-light">
            
        </footer>
        @vite(['resources/js/app.js'])
    </body>
</html>




