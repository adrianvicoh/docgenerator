@extends('layouts.index')

@section('content')
<div class="container">
    <h1 class="text-center">Formulario de incidencias</h1>

    <form action="{{ route('procesar') }}" method="post">
        @csrf
        <div class="form-group">
            <label for="nombre">Nom:</label>
            <input type="text" id="nombre" name="nombre" class="form-control" required>
        </div>

        <div class="form-group">
            <label for="apellidos">Cognoms:</label>
            <input type="text" id="apellidos" name="apellidos" class="form-control" required>
        </div>

        <div class="form-group m-3">
            <p>Als efectes de <input type="checkbox" id="checkboxNotificar" name="checkboxNotificar">notificar i/o <input type="checkbox" id="checkboxJustificar" name="checkboxJustificar">justificar la incidència que he tingut en el registre de control de compliment de la meva jornada laboral del dia <input type="date" id="fechaFalta" name="fechaFalta" required><br> Declaro:</p>
        </div>

        <div class="form-group">
            <div class="form-check">
                <input type="checkbox" id="checkbox1" name="incidencia1" value="Que m’he oblidat de marcar a l’hora que corresponia i ho he fet més tard." class="form-check-input">
                <label for="checkbox1" class="form-check-label">Que m’he oblidat de marcar a l’hora que corresponia i ho he fet més tard.</label>
            </div>

            <div class="form-check">
                <input type="checkbox" id="checkbox2" name="incidencia2" value="Que m’he oblidat de marcar a l’hora que corresponia en iniciar la jornada de treball." class="form-check-input">
                <label for="checkbox2" class="form-check-label">Que m’he oblidat de marcar a l’hora que corresponia en iniciar la jornada de treball.</label>
            </div>

            <div class="form-check">
                <input type="checkbox" id="checkbox3" name="incidencia3" value="Que m’he oblidat de marcar a l’hora que corresponia, en finalitzar la meva jornada laboral." class="form-check-input">
                <label for="checkbox3" class="form-check-label">Que m’he oblidat de marcar a l’hora que corresponia, en finalitzar la meva jornada laboral.</label>
            </div>

            <div class="form-check">
                <input type="checkbox" id="checkbox4" name="incidencia4" value="Que, en tenir una activitat complementària (junta avaluació, d’equip docent, reunió, etc.), no la he tingut en compte i no he marcat quan corresponia." class="form-check-input">
                <label for="checkbox4" class="form-check-label">Que, en tenir una activitat complementària (junta avaluació, d’equip docent, reunió, etc.), no la he tingut en compte i no he marcat quontenía i no he marcat quan corresponia.</label>
            </div>

            <div class="form-check">
                <input type="checkbox" id="checkbox5" name="incidencia5" value="Que no he marcat per anar a visitar empreses (FPCT)." class="form-check-input">
                <label for="checkbox5" class="form-check-label">Que no he marcat per anar a visitar empreses (FPCT).</label>
            </div>

            <div class="form-check">
                <input type="checkbox" id="checkbox6" name="incidencia6" value="Que no he marcat per realitzar les activitats escolars fora del Centre." class="form-check-input">
                <label for="checkbox6" class="form-check-label">Que no he marcat per realitzar les activitats escolars fora del Centre.</label>
            </div>

            <div class="form-check">
                <input type="checkbox" id="checkbox7" name="incidencia7" value="Que m’he incorporat amb retard al Centre per les següents circumstàncies:" class="form-check-input">
                <label for="retardo" class="form-check-label">Que m’he incorporat amb retard al Centre per les següents circumstàncies:</label><br>
                <textarea id="retardo" name="retardo" rows="2" cols="30" class="form-control"></textarea>
            </div>

            <div class="form-check">
                <input type="checkbox" id="checkbox8" name="incidencia8" value="Que he sortit del Centre abans de finalitzar la meva jornada laboral per les següents circumstàncies:" class="form-check-input">
                <label for="salida" class="form-check-label">Que he sortit del Centre abans de finalitzar la meva jornada laboral per les següents circumstàncies:</label><br>
                <textarea id="salida" name="salida" rows="2" cols="30" class="form-control"></textarea>
            </div>

            <div class="form-check">
                <input type="checkbox" id="checkbox9" name="incidencia9" value="Que no he marcat per les següents circumstàncies:" class="form-check-input">
                <label for="otras" class="form-check-label">Que no he marcat per les següents circumstàncies:</label><br>
                <textarea id="otras" name="otras" rows="2" cols="30" class="form-control"></textarea>
            </div>

            <div class="form-check">
                <input type="checkbox" id="checkbox10" name="incidencia10" value="Que aporto el/s següent/s document/s justificatius:" class="form-check-input">
                <label for="documentos" class="form-check-label">Que aporto el/s següent/s document/s justificatius:</label><br>
                <textarea id="documentos" name="documentos" rows="2" cols="30" class="form-control"></textarea>
            </div>
        </div>
           
        <div class="form-group m-3">
            <p>Barcelona <input type="date" id="fechaFirma" name="fechaFirma" required><br></p>
        </div>

        <div class="form-group">
            <p>Nota: Assenyaleu amb “X” la casella corresponent. La declaració s’ha de presentar a Prefectura d’Estudis el mateix dia de la incidència o sinó l’endemà, si la incidència és a la sortida.</p>
        </div>

        <div class="form-group">
            <input type="submit" value="Enviar" class="btn btn-primary">
        </div>
        </form>
    </div>

@endsection