<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use \App\Models\Form;

class FormsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Form::create([
            'name' => 'curriculum',
            'xml_path' => "storage/docs/curriculum/curriculum.xml",
            'html_path' => "storage/docs/curriculum/curriculum.html",
            'image' => "storage/docs/curriculum/curriculum.png",
        ]);
        Form::create([
            'name' => 'albaran',
            'xml_path' => "storage/docs/albaran/albaran.xml",
            'html_path' => "storage/docs/albaran/albaran.html",
            'image' => "storage/docs/albaran/albaran.png",
        ]);
        Form::create([
            'name' => 'factura',
            'xml_path' => "storage/docs/factura/factura.xml",
            'html_path' => "storage/docs/factura/factura.html",
            'image' => "storage/docs/factura/factura.png",
        ]);
    }
}
