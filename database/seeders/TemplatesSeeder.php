<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use \App\Models\Template;

class TemplatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        Template::create([
            'name' => 'curriculum1',
            'route' => "storage/docs/curriculum/templates/curriculum1/curriculum1.css",
            'image' => "storage/docs/curriculum/templates/curriculum1/curriculum1.png",
            'form_id' => 1
        ]);
        Template::create([
            'name' => 'curriculum2',
            'route' => "storage/docs/curriculum/templates/curriculum2/curriculum2.css",
            'image' => "storage/docs/curriculum/templates/curriculum2/curriculum2.png",
            'form_id' => 1
        ]);
        Template::create([
            'name' => 'albaran1',
            'route' => "storage/docs/albaran/templates/albaran1/albaran1.css",
            'image' => "storage/docs/albaran/templates/albaran1/albaran1.png",
            'form_id' => 2
        ]);
        Template::create([
            'name' => 'albaran2',
            'route' => "storage/docs/albaran/templates/albaran2/albaran2.css",
            'image' => "storage/docs/albaran/templates/albaran2/albaran2.png",
            'form_id' => 2
        ]);
        Template::create([
            'name' => 'factura1',
            'route' => "storage/docs/factura/templates/factura1/factura1.css",
            'image' => "storage/docs/factura/templates/factura1/factura1.png",
            'form_id' => 3
        ]);
        Template::create([
            'name' => 'factura2',
            'route' => "storage/docs/factura/templates/factura2/factura2.css",
            'image' => "storage/docs/factura/templates/factura2/factura2.png",
            'form_id' => 3
        ]);
    }
}
