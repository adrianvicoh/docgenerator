<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')/*->nullable(false)*/;
            $table->foreignId('template_id')/*->nullable(false)*/;
            $table->foreignId('form_id')/*->nullable(false)*/;
            $table->string('name')->default('name')/*->nullable(false)*/;
            $table->string('path_html')->default('html');
            $table->string('path_xml')->default('xml');
            $table->timestamps();        

            $table->foreign('user_id')->references(['id'])->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('template_id')->references(['id'])->on('templates')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            
            $table->foreign('form_id')->references(['id'])->on('forms')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('documents');
    }
};
